import random
from lab1 import multiplicative_congr
from math import exp, log
import matplotlib.pyplot as plt


def method_rev_func(G_rev, n):
    gen = multiplicative_congr()
    ans = []
    while n > 0:
        n -= 1
        x = next(gen)
        ans.append(G_rev(x))
    return ans

if __name__ == '__main__':
    lamba = 1
    def exp_distr(x):
        return 1 - exp(-lamba * x)

    def rev_exp_distr(x):
        return (-1 / lamba) * log(1 - x)

    plt.figure(1)
    plt.hist(method_rev_func(exp_distr, 10000), 30)
    plt.show()
