import random
import matplotlib.pyplot as plt


def square_mid(init=None):
    if init is None:
        random.seed()
        init = random.randint(1000, 9999)

    st = init
    while True:
        val = str(st * st)
        while len(val) < 8:
            val = '0' + val
        st = int(val[2: 6])

        if st == init:
            break

        yield st


def multiplicative_congr(A=None,k=1003,m=int(1e9+7)):
    if A is None:
        random.seed()
        A = random.randint(1, m - 1)

    st = A
    while True:
        st = (k * st) % m

        if st == A:
            break

        yield st / m

if __name__ == '__main__':

    cnt1, cnt2 = 10000, 10000
    y1, y2 = [], []
    for i in square_mid():
        y1.append(i)
        cnt1 -= 1
        if cnt1 == 0:
            break

    for i in multiplicative_congr():
        y2.append(i)
        cnt2 -= 1
        if cnt2 == 0:
            break

    plt.figure(1)
    plt.hist(y1, 10)
    plt.show()

    plt.figure(2)
    plt.hist(y2, 10)
    plt.show()

    pass
