from numpy import random as rnd
from matplotlib import pyplot as plt


#state identifiers
state_free = 0
state_busy = 1
state_locked = 2

#statistic data
out_requests = list()
average_time_in_system = [0.0, 0]
average_interval = [0.0, 0]


class Request(object):
    def __init__(self, in_time):
        self.in_time = in_time
        self.out_time = -1.0


class Channel(object):
    def __init__(self, gen):
        self.gen = gen

        self.state = state_free
        self.request = None
        self.end_time = 0.0

        self.statistic = [0, 0, 0]

    def process_request(self, request, in_time):
        self.request = request
        self.state = state_busy
        self.end_time = in_time + next(self.gen)

    def release_request(self):
        self.request.out_time = self.end_time
        self.state = state_free

    def lock(self):
        self.state = state_locked

    def count_statistic(self):
        self.statistic[self.state] += 1


class Store(object):
    def __init__(self, max_size):
        self.max_size = max_size
        self.requests = list()
        self.statistic = [0, 0]
        self.last_used = 0.0

    def count_statistic(self):
        self.statistic = [self.statistic[0] + len(self.requests), self.statistic[1] + 1]


class Phase(object):
    def __init__(self, channels, store):
        self.channels = channels
        self.store = store


def normal_d(mu, sigma):
    while True:
        yield rnd.normal(mu, sigma)


def uniform_d(low, high):
    while True:
        yield rnd.uniform(low, high)


def triangular_d(l, m, r):
    while True:
        yield rnd.triangular(l, m, r)


def exponential_d(lmbda):
    while True:
        yield rnd.exponential(lmbda)

if __name__ == '__main__':
    #system initial structure
    channel_counts = [5, 5, 3, 4, 4]
    channel_distributions = [normal_d(5.0, 2.0), uniform_d(3.0, 9.0), triangular_d(2.0, 3.5, 5.0), normal_d(5.0, 1.0), triangular_d(2.0, 3.5, 5.0)]
    store_sizes = [3, 3, 3, 3, 3]

    channels = [[Channel(channel_distributions[i]) for j in range(channel_counts[i])] for i in range(len(channel_counts))]
    stores = [Store(store_sizes[i]) for i in range(len(channel_counts))]
    phases = [Phase(channels[i], stores[i]) for i in range(len(channel_counts))]

    #modeling initial variables
    requests_count, current_count = 10000, 0
    rejected_requests, resolved_requests = list(), list()
    request_gen = exponential_d(1.0)
    system_time = 0.0

    request_interval = 0.0
    request_time = 0.0
    n = len(phases) - 1

    while current_count < requests_count:
        # count statictic per system-time-step
        for phase in phases:
            phase.store.count_statistic()
            for channel in phase.channels:
                channel.count_statistic()

        # check if we already can free some channels in last phase
        for channel in phases[n].channels:
            if channel.end_time <= system_time and channel.state == state_busy:
                channel.release_request()
                out_requests.append(channel.request)
                average_time_in_system[0] += (channel.request.out_time - channel.request.in_time)
                average_time_in_system[1] += 1
                average_interval[0] += out_requests[len(out_requests) - 1].out_time
                if len(out_requests) > 1:
                    average_interval[0] -= out_requests[len(out_requests) - 2].out_time

                average_interval[1] += 1
                print('Processed.')

        # if we have some requests in store then process them
        while any(channel.state == state_free for channel in phases[n].channels):
            if len(phases[n].store.requests) > 0:
                request = phases[n].store.requests[0]
                phases[n].store.requests.pop(0)
                for channel in phases[n].channels:
                    if channel.state == state_free:
                        phases[n].store.last_used = max(phases[i].store.last_used, channel.end_time)
                        channel.process_request(request, channel.end_time)
                        break
            else:
                break

        # then lets go from last to first and try to send requests to next phase
        for i in reversed(range(len(phases) - 1)):
            can_go_next = any(channel.state == state_free for channel in phases[i + 1].channels)
            can_go_next &= len(phases[i + 1].store.requests) < phases[i + 1].store.max_size

            for channel in phases[i].channels:
                if channel.state == state_free:
                    continue

                if channel.state == state_busy and channel.end_time <= system_time:
                    if can_go_next:
                        request = channel.request
                        was = False
                        for next_channel in phases[i + 1].channels:
                            if next_channel.state == state_free:
                                channel.release_request()
                                next_channel.process_request(request, channel.end_time)
                                was = True
                                break

                        if not was:
                            channel.release_request()
                            phases[i + 1].store.requests.append(request)

                    else:
                        channel.lock()

                elif channel.state == state_locked and can_go_next:
                    request = channel.request
                    was = False
                    for next_channel in phases[i + 1].channels:
                        if next_channel.state == state_free:
                            channel.end_time = next_channel.end_time
                            channel.release_request()
                            next_channel.process_request(request, next_channel.end_time)
                            was = True
                            break

                    if not was:
                        channel.end_time = phases[i + 1].store.last_used
                        channel.release_request()
                        phases[i + 1].store.requests.append(request)

            while any(channel.state == state_free for channel in phases[i].channels):
                if len(phases[i].store.requests) > 0:
                    request = phases[i].store.requests[0]
                    phases[i].store.requests.pop(0)
                    for channel in phases[i].channels:
                        if channel.state == state_free:
                            phases[i].store.last_used = max(phases[i].store.last_used, channel.end_time)
                            channel.process_request(request, channel.end_time)
                            break
                else:
                    break

        wtf = request_time <= system_time

        if request_time <= system_time:
            request = Request(request_time)

            current_count += 1
            if any(channel.state == state_free for channel in phases[0].channels):
                for channel in phases[0].channels:
                    if channel.state == state_free:
                        channel.process_request(request, channel.end_time)
                        break

            elif len(phases[0].store.requests) < phases[0].store.max_size:
                phases[0].store.requests.append(request)

            else:
                print('Rejected.')

            request_interval = next(request_gen)
            request_time += max(request_interval, 0.1)

            if system_time <= request_time:
                system_time += 0.01

        else:
            system_time += 0.01

    print('\n')
    print('Modeling finished!')
    print('3.1. Have 10000 requests')
    print('3.2. Average interval between successive requests: ', average_interval[0] / average_interval[1])
    print('3.3. Average time in system: ', average_time_in_system[0] / average_time_in_system[1])
    print('3.4. Rejection probability: ', 1 - len(out_requests) / requests_count)
    print('3.5. Phases statistic')
    for i in range(len(phases)):
        print(' ' * 4, i, ' phase statistic:')
        print(' ' * 8 + 'Average store size: ', phases[i].store.statistic[0] / phases[i].store.statistic[1])
        for channel in phases[i].channels:
            free_prob = channel.statistic[0] / sum(channel.statistic)
            busy_prob = channel.statistic[1] / sum(channel.statistic)
            locked_prob = channel.statistic[2] / sum(channel.statistic)
            print(' ' * 4 + 'Free: ', free_prob, ' Busy: ', busy_prob, ' Locked: ', locked_prob)