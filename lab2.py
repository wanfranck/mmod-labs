import random
from lab1 import multiplicative_congr


def rand_event(Pa, n):
    gen = multiplicative_congr()
    ans = [0, 0]
    expected = [(1 - Pa) * n, Pa * n]
    while n > 0:
        n -= 1
        x = next(gen)
        ans[x <= Pa] += 1
    return [ans, expected]


def rand_complex_event_indep(Pa, Pb, n):
    gen = multiplicative_congr()
    ans = [[0, 0], [0, 0]]
    expected = [
        [int((1 - Pa) * (1 - Pb) * n), int((1 - Pa) * Pb * n)],
        [int(Pa * (1 - Pb) * n), int(Pa * Pb * n)]
    ]
    while n > 0:
        n -= 1
        x = [next(gen), next(gen)]
        ans[x[0] <= Pa][x[1] <= Pb] += 1
    return [ans, expected]


def rand_complex_event_dep(Pa, Pb, Pba, n):
    gen = multiplicative_congr()
    Pbna = (Pb - Pa * Pba) / (1 - Pa)
    ans = [[0, 0], [0, 0]]
    expected = [
        [int((1 - Pa) * (1 - Pbna) * n), int((1 - Pa) * Pbna * n)],
        [int(Pa * (1 - Pba) * n), int(Pa * Pba * n)]
    ]
    while n > 0:
        n -= 1
        x = [next(gen), next(gen)]
        if x[0] <= Pa:
            ans[1][x[1] <= Pba] += 1
        else:
            ans[0][x[1] <= Pbna] += 1
    return [ans, expected]


def rand_complete_gr_event(P, n):
    gen = multiplicative_congr()
    s = [sum(P[:r + 1]) for r in range(len(P))]
    ans = [0] * len(P)
    expected = [int(p * n) for p in P]
    while n > 0:
        n -= 1
        x = next(gen)
        for ind, item in enumerate(s):
            if (ind > 0 and s[ind - 1] <= x <= s[ind]) or 0.0 <= x <= s[ind]:
                ans[ind] += 1
                break
    return [ans, expected]


def pirates(n, k):
    pr = 0.0
    our, an = (n * (1 / 6 + pr)), (n * (1 / 6 + pr))
    while our - an < (k - k / 5) / 5:
        pr += 0.01
        our, an = (n * (1 / 6 + pr)), (n * (1 / 6 - pr / 5))
        print(pr)

    print('Final')

    #Analytic method
    '''
    cnt = k / 5
    if n - cnt:
        print('Impossible to do.')
    win = [(n - cnt) / 6 for i in range(6)]

    win[0] += cnt

    print([w / n for w in win])
    '''

if __name__ == '__main__':
    print(rand_event(0.5, 10000))
    print(rand_complex_event_indep(0.5, 0.6, 10000))
    print(rand_complex_event_dep(0.5, 0.6, 0.7, 10000))
    print(rand_complete_gr_event([0.3, 0.2, 0.4, 0.1], 10000))

    pirates(1000, 100)