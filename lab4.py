import random
from lab1 import multiplicative_congr
import matplotlib.pyplot as plt


def imit_discrete(P, Y, n):
    gen = multiplicative_congr()
    ans = []
    while n > 0:
        n -= 1
        x = next(gen)

        l = 0.0
        for i, p in enumerate(P):
            if l <= x < l + p:
                ans.append(Y[i])
                break
            l += p
    return ans


if __name__ == '__main__':
    plt.figure(1)
    plt.hist(imit_discrete([0.1, 0.2, 0.3, 0.4], [0.3, 0.4, 0.2, 0.1], 10000))
    plt.show()